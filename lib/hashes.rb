# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_sizes = {}
  str.split.each do |word|
    word_sizes[word] = word.length
  end
  word_sizes
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by do |k, int|
    int
  end[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |gem, count|
      older[gem] = count
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = Hash.new(0)

  word.split("").each do |char, count|
    letters[char] += 1
  end

  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  unique_elements = {}

  arr.each do |el|
    if unique_elements.include?(el) == false
      unique_elements[el] = 1
    end
  end

  unique_elements.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  frequencies = Hash.new(0)

  numbers.each do |num|
    num.even? ? frequencies[:even] += 1 : frequencies[:odd] += 1
  end

  frequencies
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)

  string.chars.each do |letter|
    if "aeiou".include?(letter)
      vowel_count[letter] += 1
    end
  end

  vowel_count.sort_by do |vowel, count|
    count
  end[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  all_students = students.map do |student, birth_month|
    student if birth_month > 6
  end.compact

  student_combinations(all_students)
end

def student_combinations(students)
  combinations = []

  students.each_with_index do |first_student, i|
    sub_arr = students[(i + 1)..-1]
    sub_arr.each do |second_student, i|
      combinations << [first_student, second_student]
    end
  end

  combinations
end

#bertie, dottie, warren


# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  population = count_population(specimens)

  number_of_species = population.length

  return 1 if number_of_species == 1

  order_of_pop_size = population.sort_by { |species, size| size }

  smallest_population_size = order_of_pop_size[0][1]
  largest_population_size = order_of_pop_size[-1][1]

  (number_of_species ** 2) * (smallest_population_size/largest_population_size)
end

def count_population(species)
  population = Hash.new(0)

  species.each do |specimen|
    population[specimen] += 1
  end

  population
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_chars = Hash.new(0)
  vandalized_chars = Hash.new(0)
  normal_sign.chars.each do |char|
    normal_chars[char.downcase] += 1
  end
  vandalized_sign.chars.each do |char|
    vandalized_chars[char.downcase] += 1
  end

  vandalized_chars.each do |char, count|
    if count > normal_chars[char]
      return false
    end
  end
  true
end

def character_count(str)

end
